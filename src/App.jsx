import {
  Environment,
  Loader,
  Preload,
  PresentationControls,
  PerspectiveCamera,
  DeviceOrientationControls,

} from "@react-three/drei";
import { Canvas, useFrame } from "@react-three/fiber";
import { Suspense, useContext, useEffect, useRef, useState } from "react";
import { Model } from "./Vivamapagain.jsx";
import { Bloom, EffectComposer, N8AO } from "@react-three/postprocessing";
import "./globals.scss";
import { gsap } from "gsap";
import { useControls } from "leva";

import { Pointer } from "./Map_pointer_3d_icon.jsx";

const Camera = ({ above }) => {
  const ref = useRef();
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);

  const position = urlParams.get("position");
  const debug = urlParams.get("debug");
  const xPos = urlParams.get("x");
  const yPos = urlParams.get("y");

  const cameraLeftPos = {
    x: -70,
    y: 0,
    z: -45,
    yRotation: -Math.PI / 2,
  };

  const cameraRightPos = {
    x: 70,
    y: 0,
    z: -45,
    yRotation: Math.PI / 2,
  };

  let cameraDefaultPos = {
    x: 0,
    y: 0,
    z: 20,
  };

  if (!position && xPos && yPos) {
    cameraDefaultPos = {
      x: xPos,
      y: 0,
      z: yPos,
    };
  }

  let cameraControls;

  if(debug){
  cameraControls = useControls(
      "Camera position", {
        x: {
          value: 0,
          min: -100,
          max: 100,
          step: 1,
        },
        y: {
          value: 0,
          min: -100,
          max: 100,
          step: 1,
        },
    });
  }
  useEffect(() => {
    const camera = ref.current;

    if (above === false) {
      // Animation lorsque above est vrai (true)
      if (!position) {
        gsap.to(camera.position, {
          duration: 2,
          x: cameraDefaultPos.x,
          y: cameraDefaultPos.y,
          z: cameraDefaultPos.z,
          ease: "expo.inOut",
        });
      } else if (position === "left") {
        gsap.to(camera.position, {
          duration: 2,
          x: cameraLeftPos.x,
          y: 0,
          z: cameraLeftPos.z,
          ease: "expo.inOut",
        });
      } else if (position === "right") {
        gsap.to(camera.position, {
          duration: 2,
          x: cameraRightPos.x,
          y: 0,
          z: cameraRightPos.z,
          ease: "expo.inOut",
        });
      }
    } else {
      // Animation lorsque above est faux (false)
      gsap.to(camera.position, {
        duration: 2,
        x: 0,
        y: 90,
        z: -25,
        ease: "expo.inOut",
      });
      gsap.to(camera.rotation, {
        duration: 2,
        y: 0,
        x: -Math.PI / 2,
        z: 0,
        ease: "expo.inOut",
      });
    }
  }, [above]);

  useFrame(() => {
    ref.current.updateMatrixWorld();
    ref.current.lookAt(0, 0, -45);
  });

  console.log(debug)
  return (
    <>
      {debug ? <PerspectiveCamera ref={ref} position={[cameraControls.x, 0, cameraControls.y]} makeDefault fov={50} near={0.1} /> : <PerspectiveCamera ref={ref} makeDefault fov={50} near={0.1} />}
    </>
  );
};

const MapPointer = () => {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);

  const pointerRef = useRef();

  let position = urlParams.get("position");

  return (
    <Pointer
      ref={pointerRef}
      position={
        !position
          ? [0, -3, 0]
          : position == "left"
          ? [-55, -3, -45]
          : position == "right"
          ? [55, -3, -45]
          : [0, 0, 0]
      }
    />
  );
};

function App() {
  const [above, setAbove] = useState(false);

  const [activeNumber, setActiveNumber] = useState(3);

  // activeNumber = 1 => wc
  // activeNumber = 2 => restauration
  // activeNumber = 3 => emergency

  return (
    <div className="App" style={{ width: "100vw", height: "100vh" }}>
      <div className="buttons">
        <div
          className="button wc"
          onClick={(e) => {
            setActiveNumber(1);
          }}
        >
          <img src="wc.png" alt="wc" />
        </div>
        <div
          className="button restauration"
          onClick={(e) => {
            setActiveNumber(2);
          }}
        >
          <img src="restauration.png" alt="restauration" />
        </div>
        <div
          className="button emergency"
          onClick={(e) => {
            setActiveNumber(3);
          }}
        >
          <img src="cross.png" alt="emergency" />
        </div>
      </div>

      <div
        className="seeFromAbove"
        onClick={(e) => {
          setAbove(!above);
        }}
      >
        {above ? "Revenir" : "Voir depuis le ciel"}
      </div>
      <Canvas>
        <color attach="background" args={["#ebdcf7"]} />
        <Suspense fallback={null}>
          {window.innerWidth > 768 ? (
            <PresentationControls
              config={{ mass: 2, tension: 200 }}
              snap={{ mass: 2, tension: 200 }}
              rotation={[0, 0, 0]}
              polar={[-Math.PI / 3, Math.PI / 3]}
              azimuth={[-Math.PI / 1.4, Math.PI / 2]}
            >
              <Model activeNumber={activeNumber} />
              <MapPointer />
            </PresentationControls>
          ) : (
            <>
              <Model activeNumber={activeNumber} /> <MapPointer />
            </>
          )}
          <DeviceOrientationControls />
          <ambientLight intensity={0.5} />
          <Camera above={above} />
          {/* <Stats /> */}

          <Environment preset="city" />
          {/* <EffectComposer>
            <Bloom luminanceThreshold={1} luminanceSmoothing={0} kernelSize={5} height={300} />
            <N8AO/>
          </EffectComposer> */}
          <Preload all />
        </Suspense>
      </Canvas>
      <Loader />
    </div>
  );
}

export default App;
